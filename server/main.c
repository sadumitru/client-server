#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>

const int message_length = 100;

char *read_message(int sockfd) {
    int message_read_bytes;
    char *message = calloc(1, sizeof(char) * message_length + 1);
    int buffer_size = 100;
    char buffer[buffer_size];
    int total_read_message_bytes = 0;

    do {
        message_read_bytes = read(sockfd, buffer, buffer_size);
        total_read_message_bytes += message_read_bytes;

        if (sizeof(message) + message_read_bytes > total_read_message_bytes + message_length) {
            if (realloc(message, sizeof(message) + message_length) == NULL) {
                perror("Unable to allocate memory\n");
                exit(1);
            }
        }
        strncat(message, buffer, message_read_bytes);

    } while (message_read_bytes == buffer_size && message_read_bytes > 0);

    message[total_read_message_bytes] = '\0';
    return message;
}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "EndlessLoop"
int main(int argc, char *argv[]) {
    struct sockaddr_in server_address;
    struct sockaddr_in client_address;

    //Remove any old socket and create an unnamed socket for the server.
    int server_sockfd = socket(AF_INET, SOCK_STREAM, 0);
    int enable = 1;
    if (setsockopt(server_sockfd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0) { //manipulate options for socket
        perror("setsockopt(SO_REUSEADDR) failed");
    }

    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = htons(INADDR_ANY); //bound to all local interfaces
    server_address.sin_port = htons(7734);
    int server_len = sizeof(server_address);

    if (bind(server_sockfd, (struct sockaddr *) &server_address, server_len) != 0) {  //assign address to fd
        perror("Unable to bind.\n");
        return 1;
    }

    if (listen(server_sockfd, 5) != 0) { //max queue size 5; sever_sockfd - passive socket
        perror("Unable to listen\n");
        return 1;
    }

    unsigned client_len = sizeof(client_address);
    int client_sockfd = accept(server_sockfd, (struct sockaddr *) &client_address, &client_len);
    while (1) {
        printf("Client: %s connected on port: %d with scoket_fd: %d.\nServer waiting...\n\n", inet_ntoa(client_address.sin_addr),
               client_address.sin_port, client_sockfd);

        char *message = read_message(client_sockfd);
        while (strcasecmp(message, "close connection\n") != 0) {
            printf("New message:\nclient_id: %s\nclient_port: %d\nmessage: %s\n", inet_ntoa(client_address.sin_addr),
                   client_address.sin_port, message);
            write(client_sockfd, message, strlen(message));
            free(message);
            message = read_message(client_sockfd);
        }

        write(client_sockfd, "Good bye!", 10);
        printf("client %s closed the connection.\n\n", inet_ntoa(client_address.sin_addr));
        close(client_sockfd);
        client_sockfd = accept(server_sockfd, (struct sockaddr *) &client_address, &client_len);
    }
//    return 0;
}
#pragma clang diagnostic pop
