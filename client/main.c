#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>

const int message_length = 100;

char *read_message(int sockfd) {
    int message_read_bytes;
    char *message = calloc(1,sizeof(char) * message_length + 1);
    int buffer_size = 100;
    char buffer[buffer_size];
    int total_read_message_bytes = 0;

    do {
        message_read_bytes = read(sockfd, buffer, buffer_size);
        total_read_message_bytes += message_read_bytes;

        if (sizeof(message) + message_read_bytes > total_read_message_bytes + message_length) {
            if (realloc(message, sizeof(message) + message_length) == NULL) {
                perror("Unable to allocate memory\n");
                exit(1);
            }
        }

        strncat(message, buffer, message_read_bytes);
    } while ((message_read_bytes == buffer_size) && (message_read_bytes > 0));

    message[total_read_message_bytes] = '\0';
    return message;
}

int main(int argc, char *argv[]) {
    if (argc != 3) {
        perror("Invalid number of arguments.\n");
        exit(1);
    }

    struct sockaddr_in address;
    int sockfd = socket(AF_INET, SOCK_STREAM , 0);

    //Name the socket as agreed with server.
    address.sin_family = AF_INET; //ip v4
    address.sin_addr.s_addr = inet_addr(argv[1]); //ip addr
    address.sin_port = htons(strtol(argv[2], NULL, 10)); //port  //host byte order -> network byte order
    int len = sizeof(address);

    if(connect(sockfd, (struct sockaddr *)&address, len) != 0) {
        perror("Error has occurred");
        exit(1);
    }

    printf("In order to close the connection type \"close connection\".\n\n");
    char* message = malloc(sizeof (char ) * message_length + 1);

    while (strcasecmp(message, "close connection\n") != 0){
        printf("Enter a message: ");
        if (fgets(message, message_length + 1, stdin) != NULL) {
            write(sockfd, message, strlen(message)); //write to server
            char* initial_message = read_message(sockfd); //get message from server
            printf("Message from server: %s\n", initial_message); //print message
            free(initial_message);
        } else {
            perror("Unable to read message\n");
            exit(1);
        }
    }

    close(sockfd);
    printf("You have successfully closed the connection to the server.\n");
    exit(0);

}